fruit (2.1.dfsg-10) unstable; urgency=medium

  * QA upload.
  * debian/copyright: Update packaging copyright information.
  * debian/upstream/metadata: Add upstream metadata information.

 -- Manoel Ibere Wiedmer Gomes <manoelgomes@alunos.utfpr.edu.br>  Fri, 14 Jun 2024 14:19:22 -0300

fruit (2.1.dfsg-9) unstable; urgency=low

  * QA upload.

  [ Debian Janitor ]
  * Use correct machine-readable copyright file URI.
  * Use secure URI in Homepage field.
  * Fix day-of-week for changelog entry 2.1-1.
  * Update standards version to 4.5.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 13 Feb 2021 03:36:19 +0000

fruit (2.1.dfsg-8) unstable; urgency=medium

  * Bump DH level to 12
  * Update Standards-Version to 4.4.1
  * Use my d.o email address
  * d/control:
    - Add Rules-Requires-Root: no
    - Move packaging to salsa
  * d/copyright: Use secure url
  * d/copyright: Add coment about dfsg

 -- Samuel Henrique <samueloph@debian.org>  Tue, 26 Nov 2019 00:26:12 +0000

fruit (2.1.dfsg-7) unstable; urgency=medium

  * QA upload.
  * Bump DH to 9.
  * Bump Standards-Version to 3.9.8.
  * Set Debian QA Group as maintainer (see #835305).
  * wrap-and-sort -a.
  * debian/control: Homepage: Use new address, thanks to Mario Lang
    (closes: #742479).
  * debian/copyright: Switch to DEP-5.
  * debian/dirs: Remove file, upstream already creates needed dirs.
  * debian/patches:
    - 01-makefile.patch: Update to use DH flags (hardening + no-stripping).
    - 01-simple_go.patch:
      ~ Refactor short description.
      ~ Remove trailing whitespaces and remove comma from the author field.
      ~ Rename to 02-simple_go.patch.
  * debian/README.source: Create file to explain what is missing from the
    upstream's tarball, as one file got removed to comply with DFSG.
  * debian/rules:
    - Export DEB_BUILD_MAINT_OPTIONS = hardening=+all, to enable all hardening
      flags.
    - Export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed, in order to disable
      useless dependencies.
    - Remove trailing whitespace and comments.

 -- Samuel Henrique <samueloph@debian.org>  Fri, 16 Sep 2016 16:41:42 -0300

fruit (2.1.dfsg-6) unstable; urgency=low

  * Maintenance update.
  * Changes in debian/control: Standards-Version: 3.9.2
  * Changes in debian/control: Homepage:
    http://wbec-ridderkerk.nl/html/details1/Fruit.html (Closes: #638742)
  * Update to the manpage.

 -- Oliver Korff <ok@xynyx.de>  Wed, 07 Sep 2011 11:07:28 +0200

fruit (2.1.dfsg-5) unstable; urgency=low

  * Remove of README.Source file
  * Switch to dpkg-source 3.0 (quilt) format
  * Removed the knights dependencies, because knights is not longer
    available for updates. Removed related documentation accordingly.
  * Added no stripping to the Makefile.
  * New standards version: 3.9.1

 -- Oliver Korff <ok@xynyx.de>  Mon, 28 Mar 2011 13:32:15 +0200

fruit (2.1.dfsg-4) unstable; urgency=low

  * Updated standards version to 3.8.1
  * Set debhelper recommendation to >=7.0.0
  * Set compat level to 7
  * Added Homepage field to control file
  * Removed dh_clean -k from rules and added dh_prep
  * Added suggest on knights for version >=0.6-8.2

 -- Oliver Korff <ok@xynyx.de>  Sat, 06 Jun 2009 10:36:23 +0200

fruit (2.1.dfsg-3) unstable; urgency=low

  * Remove stripping of binaries (#Closes: 436875)
  * Changed standards version to 3.7.3
  * Adjusted the format for the homepage in control

 -- Oliver Korff <ok@xynyx.de>  Wed, 06 Feb 2008 20:25:32 +0100

fruit (2.1.dfsg-2) unstable; urgency=low

  * remove unnecessary dependency on libstdc++.so.6 and libgcc1

 -- Oliver Korff <ok@xynyx.de>  Sat, 20 Jan 2007 11:45:39 +0100

fruit (2.1.dfsg-1) unstable; urgency=low

  * removed crlf from documentation (closes: #334372)
  * changed version number back to the original numbering scheme
  * added the changes of the Non-maintainer upload

 -- Oliver Korff <ok@xynyx.de>  Thu, 25 May 2006 11:35:58 +0200

fruit (2.1.0.1dfsg-0.1) unstable; urgency=low

  * Non-maintainer upload
  * The package provided a binary without source, which broke the DFSG
    I had to repack the old orig.tar.gz and bumped the version number to
    2.1.0.1dfsg. Upstream only uses the first two digits so there
    should never be a versioning problem. (Closes: #350033)

 -- Steffen Joeris <steffen.joeris@skolelinux.de>  Sat, 29 Apr 2006 16:12:42 +0200

fruit (2.1-2) unstable; urgency=low

  * added uudecode to build dependencies (closes: #332533)
  * small changes to the control file (closes: #332793)
  * corrected misspelling in manpage (closes: #332640)

 -- Oliver Korff <oliver@xynyx.de>  Fri,  7 Oct 2005 19:40:10 +0200

fruit (2.1-1) unstable; urgency=low

  * Initial release  (closes: #324677)
  * repack of the original source was necessary, moved zip archive
  to tar, and renamed it
  * tried to be more  descriptive in the description fields

 -- Oliver Korff <ok@xynyx.de>  Wed, 24 Aug 2005 21:17:24 +0200
